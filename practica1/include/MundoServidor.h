//////////////////////////////////////////////////////////////////////
// MundoServidor.h Interfaz de la clase                             //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include <string>

class CMundo  
{
public:
	CMundo();
	virtual ~CMundo();	
	void Init();
	void Print(char *mensaje, int x, int y, float r, float g, float b);
		
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

//3. identificador del FIFO, atributo de la clase CMundo en el servidor
	int pipe;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos[2];

	Socket servidor;
	void GestionaConexiones();
	std::vector<Socket> conexiones;
	std::vector<std::string> nombres;

	void RecibeComandosJugador1();
	void RecibeComandosJugador2();
	int acabar;
 	pthread_t thid_hilo_conexiones,thid_hilo_comandos1,thid_hilo_comandos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
