//////////////////////////////////////////////////////////////////////
// Socket.h: interfaz de la clase                                   //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCKET_H__F0ACF86D_2491_474D_8E1E_09D4B604D51A__INCLUDED_)
#define AFX_SOCKET_H__F0ACF86D_2491_474D_8E1E_09D4B604D51A__INCLUDED_
#include <iostream>

#if !defined( __GNUC__)
	#include <winsock2.h> 
	#pragma comment (lib, "ws2_32.lib")  		
#else
	#include <sys/time.h> 
	#include <sys/types.h>    
	#include <sys/socket.h>
	#include <sys/select.h>	
	#include <unistd.h>
	#include <arpa/inet.h>
	#include <netinet/in.h>
	#include <pthread.h>
	#include <errno.h>	
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	#define SD_BOTH SHUT_RDWR
	#define closesocket(x) close(x)
	#define Sleep(x) usleep(x*1000)
#endif

class Socket  
{
public:
	Socket();
	Socket(int s);

	virtual ~Socket();
	
	//estas funciones devuelven 0 en caso de exito y -1 en caso de error
	int Connect(char ip[],int port);
	int InitServer(char ip[],int port);
	
	//devuelve un socket, el empleado realmente para la comunicacion
	//el socket devuelto podria ser invalido si el accept falla
	Socket Accept();
	void Close();

	//-1 en caso de error, el numero de bytes enviados o recibidos en caso de exito
	int Send(char cad[],int length);
	int Receive(char cad[],int length);

private:
	int sock;
	
};

#endif // !defined(AFX_SOCKET_H__F0ACF86D_2491_474D_8E1E_09D4B604D51A__INCLUDED_)
