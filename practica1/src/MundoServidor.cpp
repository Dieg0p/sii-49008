//////////////////////////////////////////////////////////////////////
// MundoServidor.cpp: implementacion de la clase                    //
//                                                                  //
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h> 
//////////////////////////////////////////////////////////////////////
// Construccion/Destruccion
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	acabar=0;
	Init();
}

CMundo::~CMundo()
{
	acabar=1;
	int i;
	for(i=0;i<conexiones.size();i++)
		conexiones[i].Close();
	servidor.Close();
	pthread_join(thid_hilo_conexiones,NULL);
	pthread_join(thid_hilo_comandos1,NULL);
	pthread_join(thid_hilo_comandos2,NULL);
	std::cout<<"Mundo terminado"<<std::endl;
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundo::Print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	//////////////////////////////
	//	AQUI EMPIEZA MI DIBUJO  //
	//////////////////////////////
	char cad[100];
	sprintf(cad,"Servidor");
	Print(cad,300,10,1,0,1);
	sprintf(cad,"Jugador1: %d",puntos[0]);
	Print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos[1]);
	Print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	for(i=0;i<nombres.size();i++)
	{
		if(i<2)
		{
			sprintf(cad,"%s %d",nombres[i].data(),puntos[i]);
			Print(cad,50,50+20*i,1,0,1);
		}
		else
		{
			sprintf(cad,"%s",nombres[i].data());
			Print(cad,50,50+20*i,1,1,1);
		}
	}
	
	//////////////////////////////
	//	AQUI TERMINA MI DIBUJO  //
	//////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos[1]++;
		
	//5.Datos al meter gol.
		char cad[150];
		sprintf(cad,"Jugador2 ha marcado. Puntuacion %d \n", puntos[1]);
		write(pipe, cad, 150);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos[0]++;
		char cad[150];
		sprintf(cad,"Jugador1 ha marcado. Puntuacion %d \n", puntos[0]);
		write(pipe, cad, 150);
	}
	for(i=conexiones.size()-1;i>=0;i--)
	{
		char cad[1000];
		sprintf(cad,"%d %f %f %f %f %f %f %f %f %f %f",
					i,esfera.centro.x,esfera.centro.y,
							jugador1.x1,jugador1.y1,
							jugador1.x2,jugador1.y2,
							jugador2.x1,jugador2.y1,
							jugador2.x2,jugador2.y2);	

		if(0>=conexiones[i].Send(cad,1000))
		{
			conexiones.erase(conexiones.begin()+i);
			nombres.erase(nombres.begin()+i);
			if(i<2)
				puntos[0]=puntos[1]=0;
		}
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;
	}
}
void* hilo_conexiones(void* d)
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}
void CMundo::GestionaConexiones()
{
      while(!acabar)
      {
            Socket s=servidor.Accept();
		char cad[100];
		s.Receive(cad,100);
		nombres.push_back(cad);
            conexiones.push_back(s);
      }
      std::cout<<"Terminando hilo conexiones"<<std::endl;
}
void* hilo_comandos1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}
void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}
void CMundo::RecibeComandosJugador1()
{
      while(!acabar)
      {
            usleep(10);
            if(conexiones.size()>=1)
            {
                  char cad[100];
                  conexiones[0].Receive(cad,100);
                  unsigned char key;
                  sscanf(cad,"%c",&key);
                  if(key=='s')jugador1.velocidad.y=-4;
                  if(key=='w')jugador1.velocidad.y=4;
            }
      }
      std::cout<<"Terminando hilo comandos jugador1"<<std::endl;
}
void CMundo::RecibeComandosJugador2()
{
      while(!acabar)
      {
            usleep(10);
            if(conexiones.size()>=2)
            {
                  char cad[100];
                  conexiones[1].Receive(cad,100);
                  unsigned char key;
                  sscanf(cad,"%c",&key);
                  if(key=='s')jugador2.velocidad.y=-4;
                  if(key=='w')jugador2.velocidad.y=4;
            }
      }
      std::cout<<"Terminando hilo comandos jugador1"<<std::endl;
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	
	servidor.InitServer("127.0.0.1",12000);

	//4. Abrir la tubería 
	pipe=open("/tmp\MiFifo1",O_WRONLY);

  	pthread_create(&thid_hilo_conexiones,NULL,hilo_conexiones,this);
	pthread_create(&thid_hilo_comandos1,NULL,hilo_comandos1,this);
	pthread_create(&thid_hilo_comandos2,NULL,hilo_comandos2,this);
}
