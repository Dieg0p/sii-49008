FIND_PACKAGE( Threads )
INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS_CLIENTE
	
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp
	MundoCliente.cpp)

SET(COMMON_SRCS_SERVIDOR 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp
	MundoServidor.cpp)

ADD_EXECUTABLE(logger Logger.cpp)

ADD_EXECUTABLE(bot Bot.cpp)
				
ADD_EXECUTABLE(cliente cliente.cpp ${COMMON_SRCS_CLIENTE})

ADD_EXECUTABLE(servidor servidor.cpp ${COMMON_SRCS_SERVIDOR})

TARGET_LINK_LIBRARIES(cliente glut GL GLU)

TARGET_LINK_LIBRARIES(servidor glut GL GLU)

target_link_libraries (servidor ${CMAKE_THREAD_LIBS_INIT})
