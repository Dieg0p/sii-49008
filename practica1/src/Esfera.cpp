//////////////////////////////////////////////////////////////////////
// Esfera.cpp: implementacion de la clase                           //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construccion/Destruccion                                         //
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.7f;
	velocidad.x=3.5;
	velocidad.y=3.5;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,25,118);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+(velocidad*t);
}
