//////////////////////////////////////////////////////////////////////
// Socket.cpp: implementacion de la clase                           //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#include "Socket.h"
#include <iostream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Socket::Socket()
{
	
}
Socket::Socket(int s):sock(s)
{
	
}

Socket::~Socket()
{

}

int Socket::Connect(char ip[],int port)
{	
	struct sockaddr_in server_address;

// Configuracion de la direccion IP de connexion al servidor
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr(ip);
	server_address.sin_port = htons(port);

//creacion del socket
	sock=socket(AF_INET, SOCK_STREAM,0);

//conexion
	int len= sizeof(server_address);
	connect(sock,(struct sockaddr *) &server_address,len);
	return 0;
}
int Socket::InitServer(char ip[],int port)
{
	struct sockaddr_in server_address;

	// Configuracion de la direccion del servidor
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr(ip);
	server_address.sin_port = htons(port);

	//creacion del socket servidor y escucha
	sock = socket(AF_INET, SOCK_STREAM, 0);
	int len = sizeof(server_address);
	bind(sock,(struct sockaddr *) &server_address,len);
	// Damos como maximo 5 puertos de conexion.
	listen(sock,5);	
	
	return 0;
}
Socket Socket::Accept()
{
	struct sockaddr_in client_address;
	//aceptacion de cliente (bloquea hasta la conexion)
	socklen_t leng = sizeof(client_address);
	int s= accept(sock,
				(struct sockaddr *)&client_address, &leng);

	return Socket(s);
}
int Socket::Send(char cad[],int length)
{
	//notese que el envio se hace por el socket de communicacion
	int l = send(sock, cad, length,0);
	return l;
}
int Socket::Receive(char cad[],int length)
{
	int r=recv(sock,cad,length,0);
	return r;
}

void Socket::Close()
{
	shutdown(sock, SHUT_RDWR);
	close(sock);
	sock=INVALID_SOCKET;	
	
}
