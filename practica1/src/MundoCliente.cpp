//////////////////////////////////////////////////////////////////////
// MundoCliente.cpp: implementacion de la clase                     //
//                                                                  //
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construccion/Destruccion
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundo::Print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y) 
		   
	//////////////////////////////
	//	AQUI EMPIEZA MI DIBUJO  //
	//////////////////////////////

	char cad[100];

	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	//////////////////////////////
	//	AQUI TERMINA MI DIBUJO  //
	//////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char cad[1000];
	cliente.Receive(cad,1000);
	sscanf(cad,"%d %f %f %f %f %f %f %f %f %f %f", 						
		&num_cliente,
		&esfera.centro.x,&esfera.centro.y,
		&jugador1.x1,&jugador1.y1,
		&jugador1.x2,&jugador1.y2,
		&jugador2.x1,&jugador2.y1,
		&jugador2.x2,&jugador2.y2);
	std::cout<< cad <<std::endl;
	datos->jugador=num_cliente;
	datos->e.centro=esfera.centro;
	datos->r1=jugador1;
	datos->r2=jugador2;

	if(num_cliente==0)
	{
		if(datos->accion==-1)
			OnKeyboardDown('s',0,0);
		if(datos->accion==1)
			OnKeyboardDown('w',0,0);
		datos->accion=0;
	}
	if(num_cliente==1)
	{
		if(datos->accion==-1)
			OnKeyboardDown('l',0,0);
		if(datos->accion==1)
			OnKeyboardDown('o',0,0);
		datos->accion=0;
	}


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[100];
	sprintf(cad,"%c",key);
	cliente.Send(cad,strlen(cad)+1);

}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	// izquierda
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	// derecha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	char nombre[100];
	printf("Introduzca su nombre: ");
	scanf("%s",nombre);
	cliente.Connect("127.0.0.1",12000);
	cliente.Send(nombre,strlen(nombre)+1);
	
	key_t mi_key=ftok("/bin/ls",12);
	int shmid =shmget(mi_key,sizeof(DatosMemCompartida),0666|IPC_CREAT);
	datos=(DatosMemCompartida*)shmat(shmid,NULL,0);
	
}
