//////////////////////////////////////////////////////////////////////
// Bot.cpp                                                          //
//                                                                  //
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <sys/shm.h>	
#include <string.h>
#include "datosMemCompartida.h"
#include <unistd.h>

int main()
{
	int i;
	key_t mi_key=ftok("/bin/ls",12);
	int shmid=shmget(mi_key,sizeof(DatosMemCompartida),0666);
	DatosMemCompartida* dat =(DatosMemCompartida*)shmat(shmid,NULL,0);
	while(1)
	{
		usleep(25000);
		if(dat->jugador==0)
		{
			if(dat->r1.y1 > dat->e.centro.y)
				dat->accion=-1;
			else if(dat->r1.y1 < dat->e.centro.y)
				dat->accion=1;
			else
			dat->accion=0;
		}
		
	}
	
	shmdt(dat);
	return 0;   
}
